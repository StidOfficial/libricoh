const messages = {
	en: {
		trays: "Trays",
		finisherTrays: "Finisher trays",
		cartridges: "Cartridges",
		totalCounter: "Total counter",
		lastUpdate: "Last update",
		name: "Name",
		ready: "Ready",
		noPaper: "No paper",
		cartridgeAlmostEmpty: "Cartridge almost empty",
		unknownToner: "Unknown toner",
		none: "None",
		state: "State",
		nameInputTray: "Name input tray",
		nameOuputTray: "Name output tray",
		paperSize: "Paper size",
		remarks: "Remarks",
		level: "Level",
		unknown: "Unknown",
		error: "Error",
		color: "Color",
		settings: "Settings",
		address: "Address",
		printerAddress: "Printer address",
		add: "Add",
		remove: "Remove",
		showError: "Show error",
		almostOutOfPaper: "Almost out of paper"
	},
	fr: {
		trays: "Bacs",
		finisherTrays: "Bacs finisseur",
		cartridges: "Cartouches",
		totalCounter: "Total compteur",
		lastUpdate: "Dernière mise à jour",
		name: "Nom",
		ready: "Prêt",
		noPaper: "Pas de papier",
		cartridgeAlmostEmpty: "Cartouche presque vide",
		unknownToner: "Toner inconnu",
		none: "Aucune",
		state: "État",
		nameInputTray: "Nom bac entrée",
		nameOuputTray: "Nom bac de sortie",
		paperSize: "Taille du papier",
		remarks: "Remarques",
		level: "Niveau",
		unknown: "Inconnu",
		error: "Erreur",
		color: "Couleur",
		settings: "Paramètres",
		address: "Adresse",
		printerAddress: "Adresse de l'imprimante",
		add: "Ajouter",
		remove: "Supprimer",
		showError: "Afficher l'erreur",
		almostOutOfPaper: "Papier quasi épuisé"
	}
}

const i18n = new VueI18n({
	locale: "en",
	messages
})

new Vue({
	i18n,
	el: "#app",
	template: `
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-1">
				<button type="button" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#settingsModal" v-on:click="list">{{ $t("settings") }}</button>
				<div class="modal" id="settingsModal" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">{{ $t("settings") }}</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<table class="table table-sm">
									<thead>
										<tr>
											<th scope="col">{{ $t("address") }}</th>
											<th scope="col" class="col-1"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">
												<input type="text" class="form-control" :placeholder="$t('printerAddress')" v-model="printerAddress">
											</th>
											<td>
												<button type="button" class="btn btn-info" v-on:click="addPrinter">{{ $t("add") }}</button>
											</td>
										</tr>
										<tr v-for="address in printersList">
											<th scope="row">{{ address }}</th>
											<td>
												<button type="button" class="btn btn-danger" v-on:click="removePrinter(address)">{{ $t("remove") }}</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 p-0">
				<table class="table table-sm">
					<thead>
						<tr>
							<th scope="col" class="col-1">{{ $t("name") }}</th>
							<th scope="col" class="col-4">{{ $t("trays") }}</th>
							<th scope="col" class="col-2">{{ $t("finisherTrays") }}</th>
							<th scope="col" class="col-2">{{ $t("cartridges") }}</th>
							<th scope="col" style="width:5%">{{ $t("totalCounter") }}</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="printer in printers">
							<th scope="row">
								<a :href="'http://' + printer.hostname" target="_blank">{{ printer.hostname }}</a>
								</br>
								<span class="badge badge-primary">{{ printer.OEMID }}</span>
								<span class="badge badge-secondary">{{ printer.model }}</span>
								<span class="badge badge-info">{{ printer.version }}</span>
								</br>
								<span class="badge badge-secondary">{{ $t("lastUpdate") }} : {{ printer.lastUpdate | formatDate }}</span>
								</br>
								<button type="button" class="btn btn-sm btn-danger" data-container="body" data-toggle="popover" data-placement="right" :data-content="printer.error" v-if="printer.error">{{ $t("showError") }}</button>
							</th>
							<td class="p-0">
								<table class="table table-sm table-striped m-0">
									<thead>
										<tr>
											<th scope="col" class="col-3">{{ $t("nameInputTray") }}</th>
											<th scope="col">{{ $t("paperSize") }}</th>
											<th scope="col" style="min-width:22%;max-width:22%">{{ $t("state") }}</th>
											<th scope="col" class="col-4">{{ $t("remarks") }}</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="inputTray in printer.inputTrays">
											<th scope="row">
												{{ inputTray.name }}
												<span class="badge float-right" :class="{ 'badge-success': (inputTray.level * 100 / inputTray.maxLevel) >= 50 || inputTray.level < 0, 'badge-warning': (inputTray.level * 100 / inputTray.maxLevel) < 50 && (inputTray.level * 100 / inputTray.maxLevel) > 0, 'badge-danger': (inputTray.level * 100 / inputTray.maxLevel) === 0 }">{{ inputTray.level }} / {{ inputTray.maxLevel }}</span>
											</th>
											<td>
												<img v-if="(inputTray.level * 100 / inputTray.maxLevel) === 0" src="images/deviceStPend16.gif">
												<img v-else-if="(inputTray.level * 100 / inputTray.maxLevel) <= 15 && inputTray.level > 0" src="images/deviceStPNend16.gif">
												<img v-else-if="(inputTray.level * 100 / inputTray.maxLevel) <= 35 && inputTray.level > 0" src="images/deviceStP25_16.gif">
												<img v-else-if="(inputTray.level * 100 / inputTray.maxLevel) <= 50 && inputTray.level > 0" src="images/deviceStP50_16.gif">
												<img v-else-if="(inputTray.level * 100 / inputTray.maxLevel) <= 75 && inputTray.level > 0" src="images/deviceStP75_16.gif">
												<img v-else-if="(inputTray.level * 100 / inputTray.maxLevel) <= 100 || inputTray.level < 0" src="images/deviceStP100_16.gif">
												<span class="badge badge-info">{{ inputTray | paperSize }}</span>
												<img v-if="inputTray.width < inputTray.height" src="images/deviceStDH.gif">
												<img v-else src="images/deviceStDV.gif">
											</td>
											<td v-if="inputTray.state === 19" class="text-danger">{{ $t("error") }}</td>
											<td v-else-if="(inputTray.level * 100 / inputTray.maxLevel) > 20 || inputTray.level < 0" class="text-success">{{ $t("ready") }}</td>
											<td v-else-if="(inputTray.level * 100 / inputTray.maxLevel) < 20 && (inputTray.level * 100 / inputTray.maxLevel) > 0" class="text-warning">{{ $t("almostOutOfPaper") }}</td>
											<td v-else-if="(inputTray.level * 100 / inputTray.maxLevel) >= 0 || inputTray.level == 0" class="text-danger">{{ $t("noPaper") }}</td>
											<td v-else class="text-warning">{{ $t("unknown") }}</td>
											<td v-if="inputTray.remark">{{ inputTray.remark }}</td>
											<td v-else>{{ $t("none") }}</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td class="p-0">
								<table class="table table-sm table-striped m-0">
									<thead>
										<tr>
											<th scope="col" class="col-9">{{ $t("nameOuputTray") }}</th>
											<th scope="col" class="col-3">{{ $t("state") }}</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="outputTray in printer.outputTrays">
											<th scope="row">
												{{ outputTray.name }}
												<span class="badge badge-secondary float-right">{{ outputTray.level }} / {{ outputTray.maxLevel}}</span>
											</th>
											<td v-if="outputTray.state === 0" class="text-success"><img src="images/deviceStOt16.gif"> {{ $t("ready") }}</td>
											<td v-else-if="outputTray.state === 19" class="text-danger"><img src="images/deviceStOtFull16.gif"> Réceptacle sortie plein</td>
											<td v-else>{{ outputTray.state }}</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td class="p-0">
								<table class="table table-sm table-striped m-0">
									<thead>
										<tr>
											<th scope="col" class="col-7">{{ $t("color") }}</th>
											<th scope="col" class="col-5">{{ $t("level") }}</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="cartridge in printer.cartridges">
											<th scope="row">{{ cartridge.name }}</th>
											<td v-if="cartridge.level >= 0">
												<div class="progress">
													<div class="progress-bar" :class="{ 'bg-secondary': cartridge.color === 3 || cartridge.color === 13, 'bg-info': cartridge.color === 10, 'bg-danger': cartridge.color === 11, 'bg-warning': cartridge.color === 12 }" role="progressbar" :style="{ width: cartridge.level + '%' }"></div>
												</div>
											</td>
											<td v-else-if="cartridge.level === -100" class="text-warning"><img src="images/deviceStTNend16.gif"> {{ $t("cartridgeAlmostEmpty") }}</td>
											<td v-else-if="cartridge.level === -3" class="text-success"><img src="images/deviceStToner_16.gif"> OK</td>
											<td v-else-if="cartridge.level === -2" class="text-danger">{{ $t("unknownToner") }}</td>
											<td v-else class="text-danger">Erreur level : {{ cartridge.level }}</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td class="text-center">{{ printer.totalCounter }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	`,
	data: {
		printers: [],
		printersList : [],
		printerAddress: "",
		timer: null
	},
	filters: {
		formatDate: function(value) {
			if(value) {
				var date = new Date(value * 1000)
				return date.toLocaleDateString() + " " + date.toLocaleTimeString()
			}
		},
		paperSize: function(tray) {
			var paperSizeTable = {
				"A0": {
					width: 841,
					height: 1189
				},
				"A1": {
					width: 594,
					height: 841
				},
				"A2": {
					width: 420,
					height: 594
				},
				"A3": {
					width: 297,
					height: 420
				},
				"A4": {
					width: 210,
					height: 297
				},
				"A5": {
					width: 148,
					height: 210
				},
				"A6": {
					width: 105,
					height: 148
				},
				"A7": {
					width: 74,
					height: 105
				},
				"A8": {
					width: 52,
					height: 74
				},
				"A9": {
					width: 37,
					height: 52
				},
				"A10": {
					width: 26,
					height: 37
				},
				
				"B0": {
					width: 1000,
					height: 1414
				},
				"B1": {
					width: 707,
					height: 1000
				},
				"B2": {
					width: 500,
					height: 707
				},
				"B3": {
					width: 353,
					height: 500
				},
				"B4": {
					width: 250,
					height: 353
				},
				"B4 JIS": {
					width: 257,
					height: 364
				},
				"B5": {
					width: 176,
					height: 250
				},
				"B6": {
					width: 125,
					height: 176
				},
				"B7": {
					width: 88,
					height: 125
				},
				"B8": {
					width: 62,
					height: 88
				},
				"B9": {
					width: 44,
					height: 62
				},
				"B10": {
					width: 31,
					height: 44
				},

				"C0": {
					width: 917,
					height: 1297
				},
				"C1": {
					width: 648,
					height: 917
				},
				"C2": {
					width: 458,
					height: 648
				},
				"C3": {
					width: 324,
					height: 458
				},
				"C4": {
					width: 229,
					height: 324
				},
				"C5": {
					width: 162,
					height: 229
				},
				"C6": {
					width: 114,
					height: 162
				},
				"C7": {
					width: 81,
					height: 114
				},
				"C8": {
					width: 57,
					height: 81
				},
				"C9": {
					width: 40,
					height: 57
				},
				"C10": {
					width: 28,
					height: 40
				},

				"8 1/2 x 14 in": {
					width: 14,
					height: 8.5
				},
				"8 1/2 x 11 in": {
					width: 11,
					height: 8.5
				},
				"11 x 17 in": {
					width: 11,
					height: 17
				},
				"12 x 18 in": {
					width: 12,
					height: 18
				}
			}
			
			if(tray && tray.hasOwnProperty("width") && tray.hasOwnProperty("height")) {
				if(tray.width < 0 || tray.height < 0)
					return "unknown"

				var strWidth = tray.width.toString()
				var strHeight = tray.height.toString()

				var commaPos = 0
				var unitName = " unknown"
				if(tray.unit == 4) {
					commaPos = 3
					unitName = " mm"
				} else if(tray.unit == 3) {
					commaPos = 4
					unitName = " in"
				}

				var width = 0
				var height = 0

				if(commaPos > 0) {
					width = parseFloat(strWidth.substr(0, strWidth.length - commaPos) + "." + strWidth.substr(strWidth.length - commaPos))
					height = parseFloat(strHeight.substr(0, strHeight.length - commaPos) + "." + strHeight.substr(strHeight.length - commaPos))
				} else {
					width = parseFloat(strWidth)
					height = parseFloat(strHeight)
				}

				for(var key in paperSizeTable) {
					var paperSize = paperSizeTable[key]
					if((paperSize.width == width && paperSize.height == height) || (paperSize.width == height && paperSize.height == width) ||
						(paperSize.width == Math.round(width) && paperSize.height == Math.round(height)) || (paperSize.width == Math.round(height) && paperSize.height == Math.round(width))) {
						return key
					}
				}
				
				return width.toFixed(1) + " x " + height.toFixed(1) + unitName
			}
		}
	},
	methods: {
		refresh: function() {
			this.$http.get("http://localhost:8000/api/printer/all").then(response => {
				this.printers = response.body
			})
		},
		list: function() {
			this.$http.get("http://localhost:8000/api/printer/list").then(response => {
				this.printersList = response.body.printers
			})
		},
		addPrinter: function() {
			this.$http.post("http://localhost:8000/api/printer/add", {address: this.printerAddress}).then(response => {
				this.list()
			})
			this.printerAddress = ""
		},
		removePrinter: function(address) {
			this.$http.post("http://localhost:8000/api/printer/remove", {address: address}).then(response => {
				this.list()
			})
		}
	},
	created: function() {
		this.refresh();
		this.timer = setInterval(this.refresh, 10000)
	},
	beforeDestroy: function() {
		clearInterval(this.timer)
	},
	i18n
})

$(function () {
    $('[data-toggle="popover"]').popover()
})
