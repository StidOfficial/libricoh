from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
import time
import json
import re
import threading
import datetime
import Ricoh.SNMP
import sqlite3

conn = sqlite3.connect("servers.db")
conn.execute("CREATE TABLE IF NOT EXISTS servers(address TEXT)")
conn.commit()

HOST_NAME = "localhost"
PORT_NUMBER = 8000

printersData = {}

for row in conn.execute("SELECT address FROM servers"):
	printerData = {}
	printerData["hostname"] = row[0]
	printerData["inputTrays"] = []
	printerData["outputTrays"] = []
	printerData["cartridges"] = []
	printerData["printLanguages"] = []
	printerData["lastUpdate"] = None
	printerData["error"] = None

	printersData[row[0]] = printerData

def xstr(s):
	if s is None:
		return None

	return str(s)

class SNMPCheck(threading.Thread):
	def run(self):
		self.wantStop = False
		while not self.wantStop:
			for key in list(printersData):
				if self.wantStop:
					break

				printerData = {}
				printerData["hostname"] = key

				try:
					ricoh = Ricoh.SNMP.RicohSNMP(key)

					system = ricoh.System()
					printerData["model"] = xstr(system.Name())
					printerData["contact"] = xstr(system.Contact())
					printerData["location"] = xstr(system.Location())
					#printerData["ProductID"] = xstr(system.ProductID())
					printerData["OEMID"] = xstr(system.OEMID())
					printerData["version"] = xstr(system.Version())
					printerData["language"] = xstr(system.Language())
					printerData["country"] = xstr(system.Country())
					printerData["MIBVersion"] = xstr(system.MIBVersion())

					inputTrays = ricoh.InputTrays()
					printerData["inputTrays"] = []
					for inputTray in inputTrays:
						inputTrayData = {}
						inputTrayData["standardName"] = xstr(inputTray.StandardName())
						inputTrayData["name"] = xstr(inputTray.Name())
						inputTrayData["level"] = int(inputTray.Level())
						inputTrayData["maxLevel"] = int(inputTray.MaxLevel())
						inputTrayData["height"] = int(inputTray.Height())
						inputTrayData["width"] = int(inputTray.Width())
						inputTrayData["remark"] = xstr(inputTray.Remark())
						inputTrayData["unit"] = int(inputTray.Unit())
						inputTrayData["unknown3"] = int(inputTray.Unknown3())
						inputTrayData["state"] = int(inputTray.State())

						printerData["inputTrays"].append(inputTrayData)

					outputTrays = ricoh.OutputTrays()
					printerData["outputTrays"] = []
					for outputTray in outputTrays:
						outputTrayData = {}
						outputTrayData["standardName"] = xstr(outputTray.StandardName())
						outputTrayData["name"] = xstr(outputTray.Name())
						outputTrayData["level"] = int(outputTray.Level())
						outputTrayData["maxLevel"] = int(outputTray.MaxLevel())
						outputTrayData["state"] = int(outputTray.State())

						printerData["outputTrays"].append(outputTrayData)

					cartridges = ricoh.Cartridges()
					printerData["cartridges"] = []
					for cartridge in cartridges:
						cartridgesData = {}
						cartridgesData["standardName"] = xstr(cartridge.StandardName())
						cartridgesData["name"] = xstr(cartridge.Name())
						cartridgesData["level"] = int(cartridge.Level())
						cartridgesData["color"] = int(cartridge.Color())

						printerData["cartridges"].append(cartridgesData)

					printLanguages = ricoh.PrintLanguages()
					printerData["printLanguages"] = []
					for printLanguage in printLanguages:
						printLanguageData = {}
						printLanguageData["version"] = xstr(printLanguage.Version())
						printLanguageData["unknown1"] = xstr(printLanguage.Unknown1())
						printLanguageData["type"] = xstr(printLanguage.Type())
						
						printerData["printLanguages"].append(printLanguageData)

					printerData["totalCounter"] = int(ricoh.TotalCounter())
					printerData["error"] = None
				except Exception as e:
					printerData["error"] = xstr(e)

				printerData["lastUpdate"] = int(datetime.datetime.timestamp(datetime.datetime.now()))

				if key in printersData:
					printersData[key] = printerData
			time.sleep(12)

	def stop(self):
		self.wantStop = True
		self.join()

SNMPCheckThread = SNMPCheck()
SNMPCheckThread.start()

class API:
	def PrinterAdd(params, args, data):
		if not data:
			return {"error": "No data"}

		address = data.get("address", None)
		if not address:
			return {"error": "No address parameter"}

		if address in printersData:
			return {"error": "This address already exist"}

		conn.execute("INSERT INTO servers(address) VALUES (?)", (address,))
		conn.commit()

		printerData = {}
		printerData["hostname"] = address
		printerData["inputTrays"] = []
		printerData["outputTrays"] = []
		printerData["cartridges"] = []
		printerData["printLanguages"] = []
		printerData["lastUpdate"] = None
		printerData["error"] = None

		printersData[address] = printerData

		return {}

	def PrinterRemove(params, args, data):
		if not data:
			return {"error": "No data"}

		address = data.get("address", None)
		if not address:
			return {"error": "No address parameter"}

		if not address in printersData:
			return {"error": "This address not exist"}

		conn.execute("DELETE FROM servers WHERE address=?", (address,))
		conn.commit()

		del printersData[address]

		return {}

	def PrinterList(params, args, data):
		return {"printers": [key for key, value in printersData.items()]}

	def PrinterAll(params, args, data):
		return printersData

	def Printer(params, args, data):
		return {"printer": printersData.get(args[0], None)}

	def PrinterInputTrays(params, args, data):
		printer = API.Printer(params, args, data)
		if printer:
			return {"inputTrays": printer["inputTrays"]}
		else:
			return {"inputTrays": None}

	def PrinterOutputTrays(params, args, data):
		printer = API.Printer(params, args, data)
		if printer:
			return {"outputTrays": printer["outputTrays"]}
		else:
			return {"outputTrays": None}

	def PrinterCartridges(params, args, data):
		printer = API.Printer(params, args, data)
		if printer:
			return {"cartridges": printer["cartridges"]}
		else:
			return {"cartridges": None}

	def PrinterPrintLanguages(params, args, data):
		printer = API.Printer(params, args, data)
		if printer:
			return {"printLanguages": printer["printLanguages"]}
		else:
			return {"printLanguages": None}

routes = {
	r"/api/printer/add": {
		"methods": ["POST"],
		"call": API.PrinterAdd
	},
	r"/api/printer/remove": {
		"methods": ["POST"],
		"call": API.PrinterRemove
	},
	r"/api/printer/list": {
		"methods": ["GET"],
		"call": API.PrinterList
	},
	r"/api/printer/all": {
		"methods": ["GET"],
		"call": API.PrinterAll
	},
	r"/api/printer/([A-Za-z0-9.]+)/inputTrays": {
		"methods": ["GET"],
		"call": API.PrinterInputTrays
	},
	r"/api/printer/([A-Za-z0-9.]+)/outputTrays": {
		"methods": ["GET"],
		"call": API.PrinterOutputTrays
	},
	r"/api/printer/([A-Za-z0-9.]+)/cartridges": {
		"methods": ["GET"],
		"call": API.PrinterCartridges
	},
	r"/api/printer/([A-Za-z0-9.]+)/printLanguages": {
		"methods": ["GET"],
		"call": API.PrinterPrintLanguages
	},
	r"/api/printer/([A-Za-z0-9.]+)": {
		"methods": ["GET"],
		"call": API.Printer
	}
}

class Server(BaseHTTPRequestHandler):
	def do_OPTIONS(self):
		self.send_response(200)
		self.send_header("Access-Control-Allow-Origin", "*")
		self.send_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		self.send_header("Access-Control-Allow-Headers", "Content-Type")
		self.end_headers()

	def do_HEAD(self):
		return

	def do_POST(self):
		contentLength = int(self.headers["Content-Length"])
		content = self.rfile.read(contentLength).decode("utf-8")

		if self.headers["Content-Type"] == "application/json" or self.headers["Content-Type"] == "application/json;charset=utf-8":
			if content:
				self.respond("POST", json.loads(content))
			else:
				self.respond("POST", None)
			return

		if content:
			self.respond("POST", content)
		else:
			self.respond("POST", None)

	def do_GET(self):
		self.respond("GET", None)

	def handle_http(self, status, contentType = None):
		self.send_response(status)
		if contentType:
			self.send_header("Content-Type", contentType)
		self.send_header("Access-Control-Allow-Origin", "*")
		self.end_headers()

	def respond(self, method, data):
		route = None
		routeContent = None
		match = None

		query = urlparse(self.path).query
		params = parse_qs(urlparse(self.path).query)

		for path in routes:
			regex = re.compile(path)
			match = regex.findall(self.path)
			if match:
				for i in range(len(match)):
					if match[i].isdigit():
						match[i] = int(match[i])
			
				route = routes[path]
				if method in route["methods"]:
					routeContent = route["call"](params, match, data)
				else:
					self.handle_http(405)
					return
				break

		if route and routeContent is not None:
			self.handle_http(200, "application/json")
			content = bytes(json.dumps(routeContent), "UTF-8")
			self.wfile.write(content)
			return

		self.handle_http(404, "application/json")
		content = bytes(json.dumps({"message": "Not found"}), "UTF-8")
		self.wfile.write(content)

if __name__ == '__main__':
	httpd = HTTPServer((HOST_NAME, PORT_NUMBER), Server)
	print(time.asctime(), 'Server UP - %s:%s' % (HOST_NAME, PORT_NUMBER))
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		pass
	httpd.server_close()

print(time.asctime(), 'Server DOWN - %s:%s' % (HOST_NAME, PORT_NUMBER))
SNMPCheckThread.stop()

conn.close()
