from pysnmp.entity.rfc3413.oneliner import cmdgen

class RicohPrintLanguage:
	def __init__(self, varBinds):
		self.__varBinds = varBinds

	def Unknown1(self):
		return self.__varBinds[0][1]

	def Type(self):
		return self.__varBinds[1][1]

	def Version(self):
		return self.__varBinds[2][1]

class RicohPrintLanguages:
	def __init__(self, ricohSNMP):
		self.__varBinds = ricohSNMP.Next(
			"1.3.6.1.2.1.43.15.1.1.4",
			"1.3.6.1.2.1.43.15.1.1.5",
			"1.3.6.1.2.1.43.15.1.1.6"
		)

		self.__printLanguages = []
		for printLanguage in self.__varBinds:
			self.__printLanguages.append(RicohPrintLanguage(printLanguage))

	def __getitem__(self, key):
		return self.__printLanguages[key]

class RicohCartridge:
	def __init__(self, varBinds):
		self.__varBinds = varBinds

	def StandardName(self):
		return self.__varBinds[0][1]

	def Name(self):
		return self.__varBinds[1][1]

	def Color(self):
		return self.__varBinds[2][1]

	def Level(self):
		return self.__varBinds[3][1]

class RicohCartridges:
	def __init__(self, ricohSNMP):
		self.__varBinds = ricohSNMP.Next(
			"1.3.6.1.4.1.367.3.2.1.2.24.1.1.2",
			"1.3.6.1.4.1.367.3.2.1.2.24.1.1.3",
			"1.3.6.1.4.1.367.3.2.1.2.24.1.1.4",
			"1.3.6.1.4.1.367.3.2.1.2.24.1.1.5"
		)

		self.__cartridges = []
		for cartridgeVarBinds in self.__varBinds:
			self.__cartridges.append(RicohCartridge(cartridgeVarBinds))

	def __getitem__(self, key):
		return self.__cartridges[key]

class RicohOutputTray:
	def __init__(self, varBinds):
		self.__varBinds = varBinds

	def Unknown0(self):
		return self.__varBinds[0][1]

	def MaxLevel(self):
		return self.__varBinds[1][1]

	def Level(self):
		return self.__varBinds[2][1]

	def State(self):
		return self.__varBinds[3][1]

	def StandardName(self):
		return self.__varBinds[4][1]

	def Name(self):
		return self.__varBinds[5][1]

class RicohOutputTrays:
	def __init__(self, ricohSNMP):
		self.__varBinds = ricohSNMP.Next(
			"1.3.6.1.2.1.43.9.2.1.3",
			"1.3.6.1.2.1.43.9.2.1.4",
			"1.3.6.1.2.1.43.9.2.1.5",
			"1.3.6.1.2.1.43.9.2.1.6",
			"1.3.6.1.2.1.43.9.2.1.7",
			"1.3.6.1.2.1.43.9.2.1.12"
		)

		self.__outputTrays = []
		for outputTrayVarBinds in self.__varBinds:
			self.__outputTrays.append(RicohOutputTray(outputTrayVarBinds))

	def __getitem__(self, key):
		return self.__outputTrays[key]

class RicohInputTray:
	def __init__(self, varBinds):
		self.__varBinds = varBinds

	def Unit(self):
		return self.__varBinds[0][1]

	def Height(self):
		return self.__varBinds[1][1]

	def Width(self):
		return self.__varBinds[2][1]

	def Unknown3(self):
		return self.__varBinds[3][1]

	def MaxLevel(self):
		return self.__varBinds[4][1]

	def Level(self):
		return self.__varBinds[5][1]

	def State(self):
		return self.__varBinds[6][1]

	def Remark(self):
		return self.__varBinds[7][1]

	def StandardName(self):
		return self.__varBinds[8][1]

	def Name(self):
		return self.__varBinds[9][1]

class RicohInputTrays:
	def __init__(self, ricohSNMP):
		self.__varBinds = ricohSNMP.Next(
			"1.3.6.1.2.1.43.8.2.1.3",
			"1.3.6.1.2.1.43.8.2.1.4",
			"1.3.6.1.2.1.43.8.2.1.5",
			"1.3.6.1.2.1.43.8.2.1.8",
			"1.3.6.1.2.1.43.8.2.1.9",
			"1.3.6.1.2.1.43.8.2.1.10",
			"1.3.6.1.2.1.43.8.2.1.11",
			"1.3.6.1.2.1.43.8.2.1.12",
			"1.3.6.1.2.1.43.8.2.1.13",
			"1.3.6.1.2.1.43.8.2.1.18"
		)

		self.__inputTrays = []
		for inputTrayVarBinds in self.__varBinds:
			self.__inputTrays.append(RicohInputTray(inputTrayVarBinds))

	def __getitem__(self, key):
		return self.__inputTrays[key]

class RicohSystem:
	def __init__(self, ricohSNMP):
		self.__varBinds = ricohSNMP.Cmd(
			"1.3.6.1.4.1.367.3.2.1.1.1.1.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.2.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.3.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.4.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.5.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.7.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.8.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.9.0",
			"1.3.6.1.4.1.367.3.2.1.1.1.10.0"
		)
        
	def Name(self):
		return self.__varBinds[0][1]

	def Version(self):
		return self.__varBinds[1][1]

	def Contact(self):
		return self.__varBinds[2][1]

	def Location(self):
		return self.__varBinds[3][1]

	def ProductID(self):
		return self.__varBinds[4][1]

	def OEMID(self):
		return self.__varBinds[5][1]

	def Language(self):
		return self.__varBinds[6][1]

	def Country(self):
		return self.__varBinds[7][1]

	def MIBVersion(self):
		return self.__varBinds[8][1]

class RicohSNMP:
	def __init__(self, address):
		self.__cmdGen = cmdgen.CommandGenerator()

		self.__publicCommunity = cmdgen.CommunityData("public", mpModel=0)
		self.__target = cmdgen.UdpTransportTarget((address, 161))

	def Cmd(self, *args):
		errorIndication, errorStatus, errorIndex, varBinds = self.__cmdGen.getCmd(self.__publicCommunity, self.__target, *args)

		if errorIndication:
			raise RuntimeError(errorIndication)
		else:
			if errorStatus:
				print("Warning : %s at %s" % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex)-1] or "?"))
				return varBinds
			else:
				return varBinds

	def Next(self, *args):
		errorIndication, errorStatus, errorIndex, varBinds = self.__cmdGen.nextCmd(self.__publicCommunity, self.__target, *args)

		if errorIndication:
			print(errorIndication)
			return []
		else:
			if errorStatus:
				print("%s at %s" % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex)-1] or "?"))
				return []
			else:
				return varBinds

	def System(self):
		return RicohSystem(self)

	def Unknown(self):
		print(self.Cmd("1.3.6.1.2.1.25.2.3.1.4.1")[0])
		print(self.Cmd("1.3.6.1.2.1.25.2.3.1.5.1")[0])
		print(self.Cmd("1.3.6.1.2.1.25.2.3.1.6.1")[0])

	def InputTrays(self):
		return RicohInputTrays(self)

	def OutputTrays(self):
		return RicohOutputTrays(self)

	def Cartridges(self):
		return RicohCartridges(self)

	def Unknown0(self):
		varBinds = self.Next(
			"1.3.6.1.4.1.367.3.2.1.2.27.2.1.2",
			"1.3.6.1.4.1.367.3.2.1.2.27.2.1.3",
			"1.3.6.1.4.1.367.3.2.1.2.27.2.1.4",
			"1.3.6.1.4.1.367.3.2.1.2.27.2.1.5"
		)

		for unknown in varBinds:
			print(unknown[0])
			print(unknown[1])
			print(unknown[2])
			print(unknown[3])

	def Network(self):
		varBinds = self.Next(
			"1.3.6.1.4.1.367.3.2.1.6.1.1.2",
			"1.3.6.1.4.1.367.3.2.1.6.1.1.4",
			"1.3.6.1.4.1.367.3.2.1.6.1.1.7",
			"1.3.6.1.4.1.367.3.2.1.6.1.1.8"
		)

		print(varBinds)

	def Functions(self):
		varBinds = self.Next("1.3.6.1.4.1.367.3.2.1.2.11.2.1.2")
		return [unknown[0][1] for unknown in varBinds]

	def PrintLanguages(self):
		return RicohPrintLanguages(self)

	def TotalCounter(self):
		varBinds = self.Next("1.3.6.1.2.1.43.10.2.1.4")
		return varBinds[0][0][1]

'''
# Example :
ricoh = RicohSNMP("MFPRICOH")

system = ricoh.System()
print("System Name :", system.Name())
print("System Version :", system.Version())
print("System Contact :", system.Contact())
print("System Location :", system.Location())
print("System Product ID :", system.ProductID())
print("System OEM ID :", system.OEMID())
print("System Language :", system.Language())
print("System Country :", system.Country())
print("System MIB Version :", system.MIBVersion())

print("---")
ricoh.Unknown()
print("---")

inputTrays = ricoh.InputTrays()
for inputTray in trays:
    print("Input tray :")
    print("Unit :", inputTray.Unit())
    print("Size :", inputTray.Height(), "x", inputTray.Width())
    print("Unknown 3 :", inputTray.Unknown3())
    print("Level :", inputTray.Level(), "/", inputTray.MaxLevel())
    print("State :", inputTray.State())
    print("Remark :", inputTray.Remark())
    print("Standard Name :", inputTray.StandardName())
    print("Name :", inputTray.Name())

outputTrays = ricoh.OutputTrays()
for outputTray in outputTrays:
    print("Output tray :")
    print("Unknown 0 :", outputTray.Unknown0())
    print("Unknown 1 :", outputTray.MaxLevel())
    print("Unknown 2 :", outputTray.Level())
    print("Unknown 3 :", outputTray.Unknown3())
    print("Standard Name :", outputTray.StandardName())
    print("Name :", outputTray.Name())

cartridges = ricoh.Cartridges()
for cartridge in cartridges:
    print("Cartridge :")
    print("Standard Name :", cartridge.StandardName())
    print("Name : ", cartridge.Name())
    print("Unknown 2 :", cartridge.Unknown2())
    print("Level :", cartridge.Level())

ricoh.Unknown0()

printLanguages = ricoh.PrintLanguages()
for printLanguage in printLanguages:
    print("Version :", printLanguage.Version())
    print("Unknown 1 :", printLanguage.Unknown1())
    print("Type :", printLanguage.Type())

print("Total counter :", ricoh.TotalCounter())
'''
